<?php

namespace Zpg\Tests;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Monolog\Logger;
use Orchestra\Testbench\TestCase;
use Zpg\GuzzleClient;
use Zpg\Interfaces\GuzzleClientInterface;
use Zpg\Interfaces\ZpgClientInterface;
use Zpg\ZpgServiceProvider;

abstract class AbstractTestCase extends TestCase
{
    /**
     * @var ZpgClientInterface
     */
    protected $client;

    /**
     * ClientTest constructor.
     */
    public function setUp(): void
    {
        parent::setUp();

        config()->set('zpg-api-client.cert', __DIR__ . '/../config/cert.crt');
        config()->set('zpg-api-client.ssl_key', __DIR__ . '/../config/private.pem');

        $this->app->extend(GuzzleClientInterface::class, function (GuzzleClientInterface $service) {
            $stack = HandlerStack::create();
            $stack->push(
                Middleware::log(
                    new Logger('Logger'),
                    new MessageFormatter('{res_body}')
                )
            );

            return new GuzzleClient(array_merge([
                'handler' => $stack,
            ], $service->getConfig()));
        });

//        dd(resolve(GuzzleClientInterface::class));

        $this->client = resolve(ZpgClientInterface::class);
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            ZpgServiceProvider::class,
        ];
    }
}
