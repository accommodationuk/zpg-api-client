<?php

return [
    "reference" => true,
    "strict" => false,
    "date-format" => "Y-m-d\TH:i:sP",
    "use-fixer" => true,
    "fixer-config-file" => null,
    "use-cacheable-supports-method" => null,
    'mapping' => [
        'https://realtime-listings.webservices.zpg.co.uk/docs/latest/schemas/branch/update.json' => [
            'root-class' => 'Branch',
            'namespace'  => 'Zpg',
            'directory'  => __DIR__ . "/../src",
        ],
        'https://realtime-listings.webservices.zpg.co.uk/docs/latest/schemas/listing/list.json' => [
            'root-class' => 'List',
            'namespace'  => 'Zpg',
            'directory'  => __DIR__ . "/../src",
        ],
        'https://realtime-listings.webservices.zpg.co.uk/docs/latest/schemas/listing/delete.json' => [
            'root-class' => 'Delete',
            'namespace'  => 'Zpg',
            'directory'  => __DIR__ . "/../src",
        ],
        'https://realtime-listings.webservices.zpg.co.uk/docs/latest/schemas/listing/update.json' => [
            'root-class' => 'Update',
            'namespace'  => 'Zpg',
            'directory'  => __DIR__ . "/../src",
        ],
        __DIR__ . '/../schemas/error.json' => [
            'root-class' => 'ErrorResponse',
            'namespace' => 'Zpg',
            'directory' => __DIR__ . '/../src',
        ],
        __DIR__ . '/../schemas/branch-update.json' => [
            'root-class' => 'BranchUpdateResponse',
            'namespace' => 'Zpg',
            'directory' => __DIR__ . '/../src',
        ],
        __DIR__ . '/../schemas/listing-update.json' => [
            'root-class' => 'ListingUpdateResponse',
            'namespace' => 'Zpg',
            'directory' => __DIR__ . '/../src',
        ],
        __DIR__ . '/../schemas/listing-delete.json' => [
            'root-class' => 'ListingDeleteResponse',
            'namespace' => 'Zpg',
            'directory' => __DIR__ . '/../src',
        ],
        __DIR__ . '/../schemas/listing-list.json' => [
            'root-class' => 'ListingListResponse',
            'namespace' => 'Zpg',
            'directory' => __DIR__ . '/../src',
        ],
    ],
];
