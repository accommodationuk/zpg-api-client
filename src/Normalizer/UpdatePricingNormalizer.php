<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdatePricingNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdatePricing';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdatePricing;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdatePricing();
        if (property_exists($data, 'auction') && $data->{'auction'} !== null) {
            $object->setAuction($data->{'auction'});
        }
        if (property_exists($data, 'currency_code') && $data->{'currency_code'} !== null) {
            $object->setCurrencyCode($data->{'currency_code'});
        }
        if (property_exists($data, 'price') && $data->{'price'} !== null) {
            $object->setPrice($data->{'price'});
        }
        if (property_exists($data, 'price_per_unit_area') && $data->{'price_per_unit_area'} !== null) {
            $object->setPricePerUnitArea($this->denormalizer->denormalize($data->{'price_per_unit_area'}, 'Zpg\\Model\\UpdatePricingPricePerUnitArea', 'json', $context));
        }
        if (property_exists($data, 'price_qualifier') && $data->{'price_qualifier'} !== null) {
            $object->setPriceQualifier($data->{'price_qualifier'});
        }
        if (property_exists($data, 'rent_frequency') && $data->{'rent_frequency'} !== null) {
            $object->setRentFrequency($data->{'rent_frequency'});
        }
        if (property_exists($data, 'transaction_type') && $data->{'transaction_type'} !== null) {
            $object->setTransactionType($data->{'transaction_type'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getAuction()) {
            $data->{'auction'} = $object->getAuction();
        }
        if (null !== $object->getCurrencyCode()) {
            $data->{'currency_code'} = $object->getCurrencyCode();
        }
        if (null !== $object->getPrice()) {
            $data->{'price'} = $object->getPrice();
        }
        if (null !== $object->getPricePerUnitArea()) {
            $data->{'price_per_unit_area'} = $this->normalizer->normalize($object->getPricePerUnitArea(), 'json', $context);
        }
        if (null !== $object->getPriceQualifier()) {
            $data->{'price_qualifier'} = $object->getPriceQualifier();
        }
        if (null !== $object->getRentFrequency()) {
            $data->{'rent_frequency'} = $object->getRentFrequency();
        }
        if (null !== $object->getTransactionType()) {
            $data->{'transaction_type'} = $object->getTransactionType();
        }
        return $data;
    }
}
