<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateAreasNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateAreas';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateAreas;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateAreas();
        if (property_exists($data, 'external') && $data->{'external'} !== null) {
            $object->setExternal($this->denormalizer->denormalize($data->{'external'}, 'Zpg\\Model\\MinMaxArea', 'json', $context));
        }
        if (property_exists($data, 'internal') && $data->{'internal'} !== null) {
            $object->setInternal($this->denormalizer->denormalize($data->{'internal'}, 'Zpg\\Model\\MinMaxArea', 'json', $context));
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getExternal()) {
            $data->{'external'} = $this->normalizer->normalize($object->getExternal(), 'json', $context);
        }
        if (null !== $object->getInternal()) {
            $data->{'internal'} = $this->normalizer->normalize($object->getInternal(), 'json', $context);
        }
        return $data;
    }
}
