<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateGoogleStreetViewNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateGoogleStreetView';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateGoogleStreetView;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateGoogleStreetView();
        if (property_exists($data, 'coordinates') && $data->{'coordinates'} !== null) {
            $object->setCoordinates($this->denormalizer->denormalize($data->{'coordinates'}, 'Zpg\\Model\\Coordinates', 'json', $context));
        }
        if (property_exists($data, 'heading') && $data->{'heading'} !== null) {
            $object->setHeading($data->{'heading'});
        }
        if (property_exists($data, 'pitch') && $data->{'pitch'} !== null) {
            $object->setPitch($data->{'pitch'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getCoordinates()) {
            $data->{'coordinates'} = $this->normalizer->normalize($object->getCoordinates(), 'json', $context);
        }
        if (null !== $object->getHeading()) {
            $data->{'heading'} = $object->getHeading();
        }
        if (null !== $object->getPitch()) {
            $data->{'pitch'} = $object->getPitch();
        }
        return $data;
    }
}
