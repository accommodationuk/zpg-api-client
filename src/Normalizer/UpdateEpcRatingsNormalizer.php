<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateEpcRatingsNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateEpcRatings';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateEpcRatings;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateEpcRatings();
        if (property_exists($data, 'eer_current_rating') && $data->{'eer_current_rating'} !== null) {
            $object->setEerCurrentRating($data->{'eer_current_rating'});
        }
        if (property_exists($data, 'eer_potential_rating') && $data->{'eer_potential_rating'} !== null) {
            $object->setEerPotentialRating($data->{'eer_potential_rating'});
        }
        if (property_exists($data, 'eir_current_rating') && $data->{'eir_current_rating'} !== null) {
            $object->setEirCurrentRating($data->{'eir_current_rating'});
        }
        if (property_exists($data, 'eir_potential_rating') && $data->{'eir_potential_rating'} !== null) {
            $object->setEirPotentialRating($data->{'eir_potential_rating'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getEerCurrentRating()) {
            $data->{'eer_current_rating'} = $object->getEerCurrentRating();
        }
        if (null !== $object->getEerPotentialRating()) {
            $data->{'eer_potential_rating'} = $object->getEerPotentialRating();
        }
        if (null !== $object->getEirCurrentRating()) {
            $data->{'eir_current_rating'} = $object->getEirCurrentRating();
        }
        if (null !== $object->getEirPotentialRating()) {
            $data->{'eir_potential_rating'} = $object->getEirPotentialRating();
        }
        return $data;
    }
}
