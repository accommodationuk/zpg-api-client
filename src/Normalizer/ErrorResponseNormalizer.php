<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ErrorResponseNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\ErrorResponse';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\ErrorResponse;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\ErrorResponse();
        if (property_exists($data, 'error_name') && $data->{'error_name'} !== null) {
            $object->setErrorName($data->{'error_name'});
        }
        if (property_exists($data, 'error_advice') && $data->{'error_advice'} !== null) {
            $object->setErrorAdvice($data->{'error_advice'});
        }
        if (property_exists($data, 'errors') && $data->{'errors'} !== null) {
            $values = array();
            foreach ($data->{'errors'} as $value) {
                $values[] = $this->denormalizer->denormalize($value, 'Zpg\\Model\\ErrorResponseErrorsItem', 'json', $context);
            }
            $object->setErrors($values);
        }
        if (property_exists($data, 'schema') && $data->{'schema'} !== null) {
            $object->setSchema($data->{'schema'});
        }
        if (property_exists($data, 'status') && $data->{'status'} !== null) {
            $object->setStatus($data->{'status'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getErrorName()) {
            $data->{'error_name'} = $object->getErrorName();
        }
        if (null !== $object->getErrorAdvice()) {
            $data->{'error_advice'} = $object->getErrorAdvice();
        }
        if (null !== $object->getErrors()) {
            $values = array();
            foreach ($object->getErrors() as $value) {
                $values[] = $this->normalizer->normalize($value, 'json', $context);
            }
            $data->{'errors'} = $values;
        }
        if (null !== $object->getSchema()) {
            $data->{'schema'} = $object->getSchema();
        }
        if (null !== $object->getStatus()) {
            $data->{'status'} = $object->getStatus();
        }
        return $data;
    }
}
