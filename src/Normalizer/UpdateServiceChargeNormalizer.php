<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateServiceChargeNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateServiceCharge';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateServiceCharge;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateServiceCharge();
        if (property_exists($data, 'charge') && $data->{'charge'} !== null) {
            $object->setCharge($data->{'charge'});
        }
        if (property_exists($data, 'per_unit_area_units') && $data->{'per_unit_area_units'} !== null) {
            $object->setPerUnitAreaUnits($data->{'per_unit_area_units'});
        }
        if (property_exists($data, 'frequency') && $data->{'frequency'} !== null) {
            $object->setFrequency($data->{'frequency'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getCharge()) {
            $data->{'charge'} = $object->getCharge();
        }
        if (null !== $object->getPerUnitAreaUnits()) {
            $data->{'per_unit_area_units'} = $object->getPerUnitAreaUnits();
        }
        if (null !== $object->getFrequency()) {
            $data->{'frequency'} = $object->getFrequency();
        }
        return $data;
    }
}
