<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class DeleteNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\Delete';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\Delete;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\Delete();
        if (property_exists($data, 'listing_reference') && $data->{'listing_reference'} !== null) {
            $object->setListingReference($data->{'listing_reference'});
        }
        if (property_exists($data, 'deletion_reason') && $data->{'deletion_reason'} !== null) {
            $object->setDeletionReason($data->{'deletion_reason'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getListingReference()) {
            $data->{'listing_reference'} = $object->getListingReference();
        }
        if (null !== $object->getDeletionReason()) {
            $data->{'deletion_reason'} = $object->getDeletionReason();
        }
        return $data;
    }
}
