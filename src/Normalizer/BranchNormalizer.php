<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class BranchNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\Branch';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\Branch;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\Branch();
        if (property_exists($data, 'branch_name') && $data->{'branch_name'} !== null) {
            $object->setBranchName($data->{'branch_name'});
        }
        if (property_exists($data, 'branch_reference') && $data->{'branch_reference'} !== null) {
            $object->setBranchReference($data->{'branch_reference'});
        }
        if (property_exists($data, 'email') && $data->{'email'} !== null) {
            $object->setEmail($data->{'email'});
        }
        if (property_exists($data, 'location') && $data->{'location'} !== null) {
            $object->setLocation($this->denormalizer->denormalize($data->{'location'}, 'Zpg\\Model\\BranchLocation', 'json', $context));
        }
        if (property_exists($data, 'telephone') && $data->{'telephone'} !== null) {
            $object->setTelephone($data->{'telephone'});
        }
        if (property_exists($data, 'website') && $data->{'website'} !== null) {
            $object->setWebsite($data->{'website'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getBranchName()) {
            $data->{'branch_name'} = $object->getBranchName();
        }
        if (null !== $object->getBranchReference()) {
            $data->{'branch_reference'} = $object->getBranchReference();
        }
        if (null !== $object->getEmail()) {
            $data->{'email'} = $object->getEmail();
        }
        if (null !== $object->getLocation()) {
            $data->{'location'} = $this->normalizer->normalize($object->getLocation(), 'json', $context);
        }
        if (null !== $object->getTelephone()) {
            $data->{'telephone'} = $object->getTelephone();
        }
        if (null !== $object->getWebsite()) {
            $data->{'website'} = $object->getWebsite();
        }
        return $data;
    }
}
