<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateLocationPafAddressNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateLocationPafAddress';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateLocationPafAddress;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateLocationPafAddress();
        if (property_exists($data, 'address_key') && $data->{'address_key'} !== null) {
            $object->setAddressKey($data->{'address_key'});
        }
        if (property_exists($data, 'organisation_key') && $data->{'organisation_key'} !== null) {
            $object->setOrganisationKey($data->{'organisation_key'});
        }
        if (property_exists($data, 'postcode_type') && $data->{'postcode_type'} !== null) {
            $object->setPostcodeType($data->{'postcode_type'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getAddressKey()) {
            $data->{'address_key'} = $object->getAddressKey();
        }
        if (null !== $object->getOrganisationKey()) {
            $data->{'organisation_key'} = $object->getOrganisationKey();
        }
        if (null !== $object->getPostcodeType()) {
            $data->{'postcode_type'} = $object->getPostcodeType();
        }
        return $data;
    }
}
