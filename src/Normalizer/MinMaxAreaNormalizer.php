<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class MinMaxAreaNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\MinMaxArea';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\MinMaxArea;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\MinMaxArea();
        if (property_exists($data, 'minimum') && $data->{'minimum'} !== null) {
            $object->setMinimum($this->denormalizer->denormalize($data->{'minimum'}, 'Zpg\\Model\\Area', 'json', $context));
        }
        if (property_exists($data, 'maximum') && $data->{'maximum'} !== null) {
            $object->setMaximum($this->denormalizer->denormalize($data->{'maximum'}, 'Zpg\\Model\\Area', 'json', $context));
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getMinimum()) {
            $data->{'minimum'} = $this->normalizer->normalize($object->getMinimum(), 'json', $context);
        }
        if (null !== $object->getMaximum()) {
            $data->{'maximum'} = $this->normalizer->normalize($object->getMaximum(), 'json', $context);
        }
        return $data;
    }
}
