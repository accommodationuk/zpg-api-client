<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateLeaseExpiryNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateLeaseExpiry';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateLeaseExpiry;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateLeaseExpiry();
        if (property_exists($data, 'expiry_date') && $data->{'expiry_date'} !== null) {
            $object->setExpiryDate($data->{'expiry_date'});
        }
        if (property_exists($data, 'years_remaining') && $data->{'years_remaining'} !== null) {
            $object->setYearsRemaining($data->{'years_remaining'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getExpiryDate()) {
            $data->{'expiry_date'} = $object->getExpiryDate();
        }
        if (null !== $object->getYearsRemaining()) {
            $data->{'years_remaining'} = $object->getYearsRemaining();
        }
        return $data;
    }
}
