<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ListingListResponseNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\ListingListResponse';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\ListingListResponse;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\ListingListResponse();
        if (property_exists($data, 'status') && $data->{'status'} !== null) {
            $object->setStatus($data->{'status'});
        }
        if (property_exists($data, 'branch_reference') && $data->{'branch_reference'} !== null) {
            $object->setBranchReference($data->{'branch_reference'});
        }
        if (property_exists($data, 'listings') && $data->{'listings'} !== null) {
            $values = array();
            foreach ($data->{'listings'} as $value) {
                $values[] = $this->denormalizer->denormalize($value, 'Zpg\\Model\\ListingListResponseListingsItem', 'json', $context);
            }
            $object->setListings($values);
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getStatus()) {
            $data->{'status'} = $object->getStatus();
        }
        if (null !== $object->getBranchReference()) {
            $data->{'branch_reference'} = $object->getBranchReference();
        }
        if (null !== $object->getListings()) {
            $values = array();
            foreach ($object->getListings() as $value) {
                $values[] = $this->normalizer->normalize($value, 'json', $context);
            }
            $data->{'listings'} = $values;
        }
        return $data;
    }
}
