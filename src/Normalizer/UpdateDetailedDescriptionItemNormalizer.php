<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateDetailedDescriptionItemNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateDetailedDescriptionItem';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateDetailedDescriptionItem;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateDetailedDescriptionItem();
        if (property_exists($data, 'dimensions') && $data->{'dimensions'} !== null) {
            $value = $data->{'dimensions'};
            if (is_object($data->{'dimensions'}) and isset($data->{'dimensions'}->{'length'}) and isset($data->{'dimensions'}->{'width'}) and (isset($data->{'dimensions'}->{'units'}) and ($data->{'dimensions'}->{'units'} == 'feet' or $data->{'dimensions'}->{'units'} == 'metres'))) {
                $value = $this->denormalizer->denormalize($data->{'dimensions'}, 'Zpg\\Model\\Dimension', 'json', $context);
            } elseif (is_string($data->{'dimensions'})) {
                $value = $data->{'dimensions'};
            }
            $object->setDimensions($value);
        }
        if (property_exists($data, 'heading') && $data->{'heading'} !== null) {
            $object->setHeading($data->{'heading'});
        }
        if (property_exists($data, 'text') && $data->{'text'} !== null) {
            $object->setText($data->{'text'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getDimensions()) {
            $value = $object->getDimensions();
            if (is_object($object->getDimensions())) {
                $value = $this->normalizer->normalize($object->getDimensions(), 'json', $context);
            } elseif (is_string($object->getDimensions())) {
                $value = $object->getDimensions();
            }
            $data->{'dimensions'} = $value;
        }
        if (null !== $object->getHeading()) {
            $data->{'heading'} = $object->getHeading();
        }
        if (null !== $object->getText()) {
            $data->{'text'} = $object->getText();
        }
        return $data;
    }
}
