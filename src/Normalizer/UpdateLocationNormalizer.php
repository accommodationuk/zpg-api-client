<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateLocationNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\UpdateLocation';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\UpdateLocation;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\UpdateLocation();
        if (property_exists($data, 'property_number_or_name') && $data->{'property_number_or_name'} !== null) {
            $value = $data->{'property_number_or_name'};
            if (is_int($data->{'property_number_or_name'})) {
                $value = $data->{'property_number_or_name'};
            } elseif (is_string($data->{'property_number_or_name'})) {
                $value = $data->{'property_number_or_name'};
            }
            $object->setPropertyNumberOrName($value);
        }
        if (property_exists($data, 'street_name') && $data->{'street_name'} !== null) {
            $object->setStreetName($data->{'street_name'});
        }
        if (property_exists($data, 'locality') && $data->{'locality'} !== null) {
            $object->setLocality($data->{'locality'});
        }
        if (property_exists($data, 'town_or_city') && $data->{'town_or_city'} !== null) {
            $object->setTownOrCity($data->{'town_or_city'});
        }
        if (property_exists($data, 'county') && $data->{'county'} !== null) {
            $object->setCounty($data->{'county'});
        }
        if (property_exists($data, 'postal_code') && $data->{'postal_code'} !== null) {
            $object->setPostalCode($data->{'postal_code'});
        }
        if (property_exists($data, 'country_code') && $data->{'country_code'} !== null) {
            $object->setCountryCode($data->{'country_code'});
        }
        if (property_exists($data, 'coordinates') && $data->{'coordinates'} !== null) {
            $object->setCoordinates($this->denormalizer->denormalize($data->{'coordinates'}, 'Zpg\\Model\\Coordinates', 'json', $context));
        }
        if (property_exists($data, 'paf_address') && $data->{'paf_address'} !== null) {
            $object->setPafAddress($this->denormalizer->denormalize($data->{'paf_address'}, 'Zpg\\Model\\UpdateLocationPafAddress', 'json', $context));
        }
        if (property_exists($data, 'paf_udprn') && $data->{'paf_udprn'} !== null) {
            $object->setPafUdprn($data->{'paf_udprn'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getPropertyNumberOrName()) {
            $value = $object->getPropertyNumberOrName();
            if (is_int($object->getPropertyNumberOrName())) {
                $value = $object->getPropertyNumberOrName();
            } elseif (is_string($object->getPropertyNumberOrName())) {
                $value = $object->getPropertyNumberOrName();
            }
            $data->{'property_number_or_name'} = $value;
        }
        if (null !== $object->getStreetName()) {
            $data->{'street_name'} = $object->getStreetName();
        }
        if (null !== $object->getLocality()) {
            $data->{'locality'} = $object->getLocality();
        }
        if (null !== $object->getTownOrCity()) {
            $data->{'town_or_city'} = $object->getTownOrCity();
        }
        if (null !== $object->getCounty()) {
            $data->{'county'} = $object->getCounty();
        }
        if (null !== $object->getPostalCode()) {
            $data->{'postal_code'} = $object->getPostalCode();
        }
        if (null !== $object->getCountryCode()) {
            $data->{'country_code'} = $object->getCountryCode();
        }
        if (null !== $object->getCoordinates()) {
            $data->{'coordinates'} = $this->normalizer->normalize($object->getCoordinates(), 'json', $context);
        }
        if (null !== $object->getPafAddress()) {
            $data->{'paf_address'} = $this->normalizer->normalize($object->getPafAddress(), 'json', $context);
        }
        if (null !== $object->getPafUdprn()) {
            $data->{'paf_udprn'} = $object->getPafUdprn();
        }
        return $data;
    }
}
