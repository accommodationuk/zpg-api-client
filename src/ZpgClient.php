<?php

namespace Zpg;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Zpg\Exceptions\ErrorResponseException;
use Zpg\Interfaces\GuzzleClientInterface;
use Zpg\Interfaces\ZpgClientInterface;
use Zpg\Model\_List;
use Zpg\Model\Branch;
use Zpg\Model\BranchUpdateResponse;
use Zpg\Model\Delete;
use Zpg\Model\ErrorResponse;
use Zpg\Model\ListingDeleteResponse;
use Zpg\Model\ListingListResponse;
use Zpg\Model\ListingUpdateResponse;
use Zpg\Model\Update;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Zpg\Normalizer\NormalizerFactory;

class ZpgClient implements ZpgClientInterface
{
    /**
     * @var GuzzleClientInterface
     */
    protected $guzzle;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * ZpgClient constructor.
     * @param GuzzleClientInterface $guzzle
     */
    public function __construct(GuzzleClientInterface $guzzle)
    {
        $this->guzzle = $guzzle;

        $normalizers = NormalizerFactory::create();
        $this->serializer = new Serializer($normalizers, [
            new JsonEncoder(
                new JsonEncode([
                    JsonEncode::OPTIONS => \JSON_UNESCAPED_SLASHES
                ]),
                new JsonDecode([
                    JsonDecode::ASSOCIATIVE => false
                ])
            ),
        ]);
    }

    /**
     * @return GuzzleClientInterface
     */
    public function getGuzzle()
    {
        return $this->guzzle;
    }

    /**
     * @param string $uri
     * @param object $model
     * @param array $headers
     * @return ResponseInterface
     * @throws ErrorResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function call(string $uri, object $model, array $headers = []): ResponseInterface
    {
        try {
            $response = $this->guzzle->request('POST', $uri, [
                'json'    => $this->serializer->normalize($model),
                'headers' => array_merge([
                    'Content-Type' => sprintf(
                        'application/json; profile=%s',
                        "http://realtime-listings.webservices.zpg.co.uk/docs/v1.2/schemas/$uri.json"
                    )
                ], $headers),
            ]);
        } catch (RequestException $exception) {
            if ($exception->getResponse()) {
                /**
                 * @var ErrorResponse $model
                 */
                $model = $this->serializer->deserialize(
                    (string)$exception->getResponse()->getBody(),
                    ErrorResponse::class,
                    'json'
                );

                throw new ErrorResponseException($model, $exception);
            }

            throw $exception;
        }

        return $response;
    }

    /**
     * @param Update $model
     * @param string|null $eTag
     * @return ListingUpdateResponse
     * @throws ErrorResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function create(Update $model, string $eTag = null): ListingUpdateResponse
    {
        return $this->update($model, $eTag);
    }

    /**
     * @param Update $model
     * @param string|null $eTag
     * @return ListingUpdateResponse
     * @throws ErrorResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function update(Update $model, string $eTag = null): ListingUpdateResponse
    {
        if (!$eTag) {
            $eTag = time();
        }

        $response = $this->call('listing/update', $model, [
            'ZPG-Listing-ETag' => $eTag,
        ]);

        return $this->serializer->deserialize(
            (string) $response->getBody(),
            ListingUpdateResponse::class,
            'json'
        );
    }

    /**
     * @param Delete $model
     * @return ListingDeleteResponse
     * @throws ErrorResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function delete(Delete $model): ListingDeleteResponse
    {
        return $this->serializer->deserialize(
            (string) $this->call('listing/delete', $model)->getBody(),
            ListingDeleteResponse::class,
            'json'
        );
    }

    /**
     * @param _List $model
     * @return ListingListResponse
     * @throws ErrorResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function list(_List $model): ListingListResponse
    {
        return $this->serializer->deserialize(
            (string) $this->call('listing/list', $model)->getBody(),
            ListingListResponse::class,
            'json'
        );
    }

    /**
     * @param Branch $model
     * @return BranchUpdateResponse
     * @throws ErrorResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function branch(Branch $model): BranchUpdateResponse
    {
        return $this->serializer->deserialize(
            (string) $this->call('branch/update', $model)->getBody(),
            BranchUpdateResponse::class,
            'json'
        );
    }
}
