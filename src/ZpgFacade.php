<?php

namespace Zpg;

use Illuminate\Support\Facades\Facade;

class ZpgFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'zpg';
    }
}
