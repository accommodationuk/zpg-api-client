<?php

namespace Zpg\Exceptions;

use GuzzleHttp\Exception\RequestException;
use Throwable;
use Zpg\Model\ErrorResponse;

class ErrorResponseException extends RequestException
{
    /**
     * @var ErrorResponse
     */
    protected $model;

    /**
     * ErrorResponseException constructor.
     * @param ErrorResponse $model
     * @param Throwable|null $previous
     */
    public function __construct(ErrorResponse $model, RequestException $previous = null)
    {
        $this->model = $model;
        parent::__construct($model->getErrorAdvice(), $previous->getRequest(), $previous->getResponse(), $previous);
    }

    /**
     * @return ErrorResponse
     */
    public function getModel(): ErrorResponse
    {
        return $this->model;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->model->getErrors();
    }
}
