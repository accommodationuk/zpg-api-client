<?php

namespace Zpg\Interfaces;

use Zpg\Model\_List;
use Zpg\Model\Branch;
use Zpg\Model\Update;
use Zpg\Model\Delete;
use Zpg\Model\BranchUpdateResponse;
use Zpg\Model\ListingDeleteResponse;
use Zpg\Model\ListingListResponse;
use Zpg\Model\ListingUpdateResponse;

interface ZpgClientInterface
{
    /**
     * @param Update $model
     * @param string $eTag
     * @return ListingUpdateResponse
     */
    public function create(Update $model, string $eTag = null): ListingUpdateResponse;

    /**
     * @param Update $model
     * @param string $eTag
     * @return ListingUpdateResponse
     */
    public function update(Update $model, string $eTag = null): ListingUpdateResponse;

    /**
     * @param Delete $model
     * @return ListingDeleteResponse
     */
    public function delete(Delete $model): ListingDeleteResponse;

    /**
     * @param _List $model
     * @return ListingListResponse
     */
    public function list(_List $model): ListingListResponse;

    /**
     * @param Branch $model
     * @return BranchUpdateResponse
     */
    public function branch(Branch $model): BranchUpdateResponse;
}
