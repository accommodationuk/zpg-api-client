<?php

namespace Zpg;

use Illuminate\Support\ServiceProvider;
use Zpg\Interfaces\GuzzleClientInterface;
use Zpg\Interfaces\ZpgClientInterface;

class ZpgServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/zpg-api-client.php',
            'zpg-api-client'
        );

        $this->app->singleton(GuzzleClientInterface::class, function () {
            return new GuzzleClient(array_merge([
                'base_uri' => $this->getUri(),
            ], config('zpg-api-client')));
        });

        $this->app->singleton(ZpgClientInterface::class, function () {
            return app(ZpgClient::class);
        });
    }

    /**
     * @return string
     */
    private function getUri()
    {
        if ($this->app->environment('production')) {
            return 'https://realtime-listings-api.webservices.zpg.co.uk/live/v1/';
        }

        return 'https://realtime-listings-api.webservices.zpg.co.uk/sandbox/v1/';
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/zpg-api-client.php' => config_path('zpg-api-client.php'),
        ], 'zpg-api-client');
    }
}
