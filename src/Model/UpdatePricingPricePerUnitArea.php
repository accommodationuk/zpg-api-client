<?php

namespace Zpg\Model;

class UpdatePricingPricePerUnitArea
{
    /**
     *
     *
     * @var mixed
     */
    protected $units;
    /**
     *
     *
     * @var float
     */
    protected $price;

    /**
     *
     *
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     *
     *
     * @param mixed $units
     *
     * @return self
     */
    public function setUnits($units): self
    {
        $this->units = $units;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     *
     *
     * @param float|null $price
     *
     * @return self
     */
    public function setPrice(?float $price): self
    {
        $this->price = $price;
        return $this;
    }
}
