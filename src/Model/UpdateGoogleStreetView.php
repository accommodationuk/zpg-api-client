<?php

namespace Zpg\Model;

class UpdateGoogleStreetView
{
    /**
     *
     *
     * @var Coordinates
     */
    protected $coordinates;
    /**
     *
     *
     * @var float
     */
    protected $heading;
    /**
     *
     *
     * @var float
     */
    protected $pitch;

    /**
     *
     *
     * @return Coordinates|null
     */
    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    /**
     *
     *
     * @param Coordinates|null $coordinates
     *
     * @return self
     */
    public function setCoordinates(?Coordinates $coordinates): self
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getHeading(): ?float
    {
        return $this->heading;
    }

    /**
     *
     *
     * @param float|null $heading
     *
     * @return self
     */
    public function setHeading(?float $heading): self
    {
        $this->heading = $heading;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getPitch(): ?float
    {
        return $this->pitch;
    }

    /**
     *
     *
     * @param float|null $pitch
     *
     * @return self
     */
    public function setPitch(?float $pitch): self
    {
        $this->pitch = $pitch;
        return $this;
    }
}
