<?php

namespace Zpg\Model;

class UpdateAreas
{
    /**
     *
     *
     * @var MinMaxArea
     */
    protected $external;
    /**
     *
     *
     * @var MinMaxArea
     */
    protected $internal;

    /**
     *
     *
     * @return MinMaxArea|null
     */
    public function getExternal(): ?MinMaxArea
    {
        return $this->external;
    }

    /**
     *
     *
     * @param MinMaxArea|null $external
     *
     * @return self
     */
    public function setExternal(?MinMaxArea $external): self
    {
        $this->external = $external;
        return $this;
    }

    /**
     *
     *
     * @return MinMaxArea|null
     */
    public function getInternal(): ?MinMaxArea
    {
        return $this->internal;
    }

    /**
     *
     *
     * @param MinMaxArea|null $internal
     *
     * @return self
     */
    public function setInternal(?MinMaxArea $internal): self
    {
        $this->internal = $internal;
        return $this;
    }
}
