<?php

namespace Zpg\Model;

class ListingDeleteResponse
{
    /**
     *
     *
     * @var string
     */
    protected $status;
    /**
     *
     *
     * @var string
     */
    protected $listingReference;

    /**
     *
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     *
     *
     * @param string|null $status
     *
     * @return self
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getListingReference(): ?string
    {
        return $this->listingReference;
    }

    /**
     *
     *
     * @param string|null $listingReference
     *
     * @return self
     */
    public function setListingReference(?string $listingReference): self
    {
        $this->listingReference = $listingReference;
        return $this;
    }
}
