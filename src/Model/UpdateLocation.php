<?php

namespace Zpg\Model;

class UpdateLocation
{
    /**
     *
     *
     * @var int|string
     */
    protected $propertyNumberOrName;
    /**
     *
     *
     * @var string
     */
    protected $streetName;
    /**
     *
     *
     * @var string
     */
    protected $locality;
    /**
     *
     *
     * @var string
     */
    protected $townOrCity;
    /**
     *
     *
     * @var string
     */
    protected $county;
    /**
     *
     *
     * @var string
     */
    protected $postalCode;
    /**
     *
     *
     * @var string
     */
    protected $countryCode;
    /**
     *
     *
     * @var Coordinates
     */
    protected $coordinates;
    /**
     *
     *
     * @var UpdateLocationPafAddress
     */
    protected $pafAddress;
    /**
     *
     *
     * @var string
     */
    protected $pafUdprn;

    /**
     *
     *
     * @return int|string|null
     */
    public function getPropertyNumberOrName()
    {
        return $this->propertyNumberOrName;
    }

    /**
     *
     *
     * @param int|string|null $propertyNumberOrName
     *
     * @return self
     */
    public function setPropertyNumberOrName($propertyNumberOrName): self
    {
        $this->propertyNumberOrName = $propertyNumberOrName;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    /**
     *
     *
     * @param string|null $streetName
     *
     * @return self
     */
    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getLocality(): ?string
    {
        return $this->locality;
    }

    /**
     *
     *
     * @param string|null $locality
     *
     * @return self
     */
    public function setLocality(?string $locality): self
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getTownOrCity(): ?string
    {
        return $this->townOrCity;
    }

    /**
     *
     *
     * @param string|null $townOrCity
     *
     * @return self
     */
    public function setTownOrCity(?string $townOrCity): self
    {
        $this->townOrCity = $townOrCity;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getCounty(): ?string
    {
        return $this->county;
    }

    /**
     *
     *
     * @param string|null $county
     *
     * @return self
     */
    public function setCounty(?string $county): self
    {
        $this->county = $county;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     *
     *
     * @param string|null $postalCode
     *
     * @return self
     */
    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     *
     *
     * @param string|null $countryCode
     *
     * @return self
     */
    public function setCountryCode(?string $countryCode): self
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     *
     *
     * @return Coordinates|null
     */
    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    /**
     *
     *
     * @param Coordinates|null $coordinates
     *
     * @return self
     */
    public function setCoordinates(?Coordinates $coordinates): self
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     *
     *
     * @return UpdateLocationPafAddress|null
     */
    public function getPafAddress(): ?UpdateLocationPafAddress
    {
        return $this->pafAddress;
    }

    /**
     *
     *
     * @param UpdateLocationPafAddress|null $pafAddress
     *
     * @return self
     */
    public function setPafAddress(?UpdateLocationPafAddress $pafAddress): self
    {
        $this->pafAddress = $pafAddress;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getPafUdprn(): ?string
    {
        return $this->pafUdprn;
    }

    /**
     *
     *
     * @param string|null $pafUdprn
     *
     * @return self
     */
    public function setPafUdprn(?string $pafUdprn): self
    {
        $this->pafUdprn = $pafUdprn;
        return $this;
    }
}
