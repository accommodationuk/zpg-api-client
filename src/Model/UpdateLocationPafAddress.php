<?php

namespace Zpg\Model;

class UpdateLocationPafAddress
{
    public const POSTCODE_TYPE_L = 'L', POSTCODE_TYPE_S = 'S';
    /**
     *
     *
     * @var string
     */
    protected $addressKey;
    /**
     *
     *
     * @var string
     */
    protected $organisationKey;
    /**
     *
     *
     * @var mixed
     */
    protected $postcodeType;

    /**
     *
     *
     * @return string|null
     */
    public function getAddressKey(): ?string
    {
        return $this->addressKey;
    }

    /**
     *
     *
     * @param string|null $addressKey
     *
     * @return self
     */
    public function setAddressKey(?string $addressKey): self
    {
        $this->addressKey = $addressKey;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getOrganisationKey(): ?string
    {
        return $this->organisationKey;
    }

    /**
     *
     *
     * @param string|null $organisationKey
     *
     * @return self
     */
    public function setOrganisationKey(?string $organisationKey): self
    {
        $this->organisationKey = $organisationKey;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getPostcodeType()
    {
        return $this->postcodeType;
    }

    /**
     *
     *
     * @param mixed $postcodeType
     *
     * @return self
     */
    public function setPostcodeType($postcodeType): self
    {
        $this->postcodeType = $postcodeType;
        return $this;
    }
}
