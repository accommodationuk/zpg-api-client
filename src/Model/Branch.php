<?php

namespace Zpg\Model;

class Branch
{
    /**
     *
     *
     * @var string
     */
    protected $branchName;
    /**
     *
     *
     * @var string
     */
    protected $branchReference;
    /**
     *
     *
     * @var string
     */
    protected $email;
    /**
     *
     *
     * @var BranchLocation
     */
    protected $location;
    /**
     *
     *
     * @var string
     */
    protected $telephone;
    /**
     *
     *
     * @var string
     */
    protected $website;

    /**
     *
     *
     * @return string|null
     */
    public function getBranchName(): ?string
    {
        return $this->branchName;
    }

    /**
     *
     *
     * @param string|null $branchName
     *
     * @return self
     */
    public function setBranchName(?string $branchName): self
    {
        $this->branchName = $branchName;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getBranchReference(): ?string
    {
        return $this->branchReference;
    }

    /**
     *
     *
     * @param string|null $branchReference
     *
     * @return self
     */
    public function setBranchReference(?string $branchReference): self
    {
        $this->branchReference = $branchReference;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     *
     *
     * @param string|null $email
     *
     * @return self
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     *
     *
     * @return BranchLocation|null
     */
    public function getLocation(): ?BranchLocation
    {
        return $this->location;
    }

    /**
     *
     *
     * @param BranchLocation|null $location
     *
     * @return self
     */
    public function setLocation(?BranchLocation $location): self
    {
        $this->location = $location;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     *
     *
     * @param string|null $telephone
     *
     * @return self
     */
    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     *
     *
     * @param string|null $website
     *
     * @return self
     */
    public function setWebsite(?string $website): self
    {
        $this->website = $website;
        return $this;
    }
}
