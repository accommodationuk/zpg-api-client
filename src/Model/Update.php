<?php

namespace Zpg\Model;

class Update
{
    public const CATEGORY_COMMERCIAL = 'commercial', CATEGORY_RESIDENTIAL = 'residential';
    public const CENTRAL_HEATING_FULL = 'full', CENTRAL_HEATING_PARTIAL = 'partial', CENTRAL_HEATING_NONE = 'none';
    public const COUNCIL_TAX_BAND_A = 'A', COUNCIL_TAX_BAND_B = 'B', COUNCIL_TAX_BAND_C = 'C', COUNCIL_TAX_BAND_D = 'D', COUNCIL_TAX_BAND_E = 'E', COUNCIL_TAX_BAND_F = 'F', COUNCIL_TAX_BAND_G = 'G', COUNCIL_TAX_BAND_H = 'H', COUNCIL_TAX_BAND_I = 'I';
    public const DECORATIVE_CONDITION_EXCELLENT = 'excellent', DECORATIVE_CONDITION_GOOD = 'good', DECORATIVE_CONDITION_AVERAGE = 'average', DECORATIVE_CONDITION_NEEDS_MODERNISATION = 'needs_modernisation';
    public const FURNISHED_STATE_FURNISHED = 'furnished', FURNISHED_STATE_FURNISHED_OR_UNFURNISHED = 'furnished_or_unfurnished', FURNISHED_STATE_PART_FURNISHED = 'part_furnished', FURNISHED_STATE_UNFURNISHED = 'unfurnished';
    public const LIFE_CYCLE_STATUS_AVAILABLE = 'available', LIFE_CYCLE_STATUS_UNDER_OFFER = 'under_offer', LIFE_CYCLE_STATUS_SOLD_SUBJECT_TO_CONTRACT = 'sold_subject_to_contract', LIFE_CYCLE_STATUS_SOLD = 'sold', LIFE_CYCLE_STATUS_LET_AGREED = 'let_agreed', LIFE_CYCLE_STATUS_LET = 'let';
    public const LISTED_BUILDING_GRADE_CATEGORY_A = 'category_a', LISTED_BUILDING_GRADE_CATEGORY_B = 'category_b', LISTED_BUILDING_GRADE_CATEGORY_C = 'category_c', LISTED_BUILDING_GRADE_GRADE_A = 'grade_a', LISTED_BUILDING_GRADE_GRADE_B = 'grade_b', LISTED_BUILDING_GRADE_GRADE_B_PLUS = 'grade_b_plus', LISTED_BUILDING_GRADE_GRADE_ONE = 'grade_one', LISTED_BUILDING_GRADE_GRADE_TWO = 'grade_two', LISTED_BUILDING_GRADE_GRADE_TWO_STAR = 'grade_two_star', LISTED_BUILDING_GRADE_LOCALLY_LISTED = 'locally_listed';
    public const PROPERTY_TYPE_BARN_CONVERSION = 'barn_conversion', PROPERTY_TYPE_BLOCK_OF_FLATS = 'block_of_flats', PROPERTY_TYPE_BUNGALOW = 'bungalow', PROPERTY_TYPE_BUSINESS_PARK = 'business_park', PROPERTY_TYPE_CHALET = 'chalet', PROPERTY_TYPE_CHATEAU = 'chateau', PROPERTY_TYPE_COTTAGE = 'cottage', PROPERTY_TYPE_COUNTRY_HOUSE = 'country_house', PROPERTY_TYPE_DETACHED = 'detached', PROPERTY_TYPE_DETACHED_BUNGALOW = 'detached_bungalow', PROPERTY_TYPE_END_TERRACE = 'end_terrace', PROPERTY_TYPE_EQUESTRIAN = 'equestrian', PROPERTY_TYPE_FARM = 'farm', PROPERTY_TYPE_FARMHOUSE = 'farmhouse', PROPERTY_TYPE_FINCA = 'finca', PROPERTY_TYPE_FLAT = 'flat', PROPERTY_TYPE_HOTEL = 'hotel', PROPERTY_TYPE_HOUSEBOAT = 'houseboat', PROPERTY_TYPE_INDUSTRIAL = 'industrial', PROPERTY_TYPE_LAND = 'land', PROPERTY_TYPE_LEISURE = 'leisure', PROPERTY_TYPE_LIGHT_INDUSTRIAL = 'light_industrial', PROPERTY_TYPE_LINK_DETACHED = 'link_detached', PROPERTY_TYPE_LODGE = 'lodge', PROPERTY_TYPE_LONGERE = 'longere', PROPERTY_TYPE_MAISONETTE = 'maisonette', PROPERTY_TYPE_MEWS = 'mews', PROPERTY_TYPE_OFFICE = 'office', PROPERTY_TYPE_PARK_HOME = 'park_home', PROPERTY_TYPE_PARKING = 'parking', PROPERTY_TYPE_PUB_BAR = 'pub_bar', PROPERTY_TYPE_RESTAURANT = 'restaurant', PROPERTY_TYPE_RETAIL = 'retail', PROPERTY_TYPE_RIAD = 'riad', PROPERTY_TYPE_SEMI_DETACHED = 'semi_detached', PROPERTY_TYPE_SEMI_DETACHED_BUNGALOW = 'semi_detached_bungalow', PROPERTY_TYPE_STUDIO = 'studio', PROPERTY_TYPE_TERRACED = 'terraced', PROPERTY_TYPE_TERRACED_BUNGALOW = 'terraced_bungalow', PROPERTY_TYPE_TOWN_HOUSE = 'town_house', PROPERTY_TYPE_VILLA = 'villa', PROPERTY_TYPE_WAREHOUSE = 'warehouse';
    public const TENURE_FEUDAL = 'feudal', TENURE_FREEHOLD = 'freehold', TENURE_LEASEHOLD = 'leasehold', TENURE_SHARE_OF_FREEHOLD = 'share_of_freehold';
    /**
     *
     *
     * @var bool
     */
    protected $accessibility;
    /**
     *
     *
     * @var string
     */
    protected $administrationFees;
    /**
     *
     *
     * @var float
     */
    protected $annualBusinessRates;
    /**
     *
     *
     * @var UpdateAreas
     */
    protected $areas;
    /**
     *
     *
     * @var int
     */
    protected $availableBedrooms;
    /**
     *
     *
     * @var string
     */
    protected $availableFromDate;
    /**
     *
     *
     * @var bool
     */
    protected $basement;
    /**
     *
     *
     * @var int
     */
    protected $bathrooms;
    /**
     *
     *
     * @var mixed[]
     */
    protected $billsIncluded;
    /**
     *
     *
     * @var string
     */
    protected $branchReference;
    /**
     *
     *
     * @var bool
     */
    protected $burglarAlarm;
    /**
     *
     *
     * @var bool
     */
    protected $businessForSale;
    /**
     *
     *
     * @var mixed[]
     */
    protected $buyerIncentives;
    /**
     *
     *
     * @var mixed
     */
    protected $category;
    /**
     *
     *
     * @var mixed
     */
    protected $centralHeating;
    /**
     *
     *
     * @var bool
     */
    protected $chainFree;
    /**
     *
     *
     * @var string[]
     */
    protected $commercialUseClasses;
    /**
     *
     *
     * @var mixed[]
     */
    protected $connectedUtilities;
    /**
     *
     *
     * @var bool
     */
    protected $conservatory;
    /**
     *
     *
     * @var int
     */
    protected $constructionYear;
    /**
     *
     *
     * @var UpdateContentItem[]
     */
    protected $content;
    /**
     *
     *
     * @var string
     */
    protected $councilTaxBand;
    /**
     *
     *
     * @var mixed
     */
    protected $decorativeCondition;
    /**
     *
     *
     * @var float
     */
    protected $deposit;
    /**
     *
     *
     * @var UpdateDetailedDescriptionItem[]
     */
    protected $detailedDescription;
    /**
     *
     *
     * @var string
     */
    protected $displayAddress;
    /**
     *
     *
     * @var bool
     */
    protected $doubleGlazing;
    /**
     *
     *
     * @var UpdateEpcRatings
     */
    protected $epcRatings;
    /**
     *
     *
     * @var string[]
     */
    protected $featureList;
    /**
     *
     *
     * @var bool
     */
    protected $fireplace;
    /**
     *
     *
     * @var bool
     */
    protected $fishingRights;
    /**
     *
     *
     * @var int[]|string[]
     */
    protected $floorLevels;
    /**
     *
     *
     * @var int
     */
    protected $floors;
    /**
     *
     *
     * @var mixed
     */
    protected $furnishedState;
    /**
     *
     *
     * @var UpdateGoogleStreetView
     */
    protected $googleStreetView;
    /**
     *
     *
     * @var float
     */
    protected $groundRent;
    /**
     *
     *
     * @var bool
     */
    protected $gym;
    /**
     *
     *
     * @var UpdateLeaseExpiry
     */
    protected $leaseExpiry;
    /**
     *
     *
     * @var mixed
     */
    protected $lifeCycleStatus;
    /**
     *
     *
     * @var mixed
     */
    protected $listedBuildingGrade;
    /**
     *
     *
     * @var string
     */
    protected $listingReference;
    /**
     *
     *
     * @var UpdateLocation
     */
    protected $location;
    /**
     *
     *
     * @var int
     */
    protected $livingRooms;
    /**
     *
     *
     * @var bool
     */
    protected $loft;
    /**
     *
     *
     * @var bool
     */
    protected $newHome;
    /**
     *
     *
     * @var string
     */
    protected $openDay;
    /**
     *
     *
     * @var bool
     */
    protected $outbuildings;
    /**
     *
     *
     * @var mixed[]
     */
    protected $outsideSpace;
    /**
     *
     *
     * @var mixed[]
     */
    protected $parking;
    /**
     *
     *
     * @var bool
     */
    protected $petsAllowed;
    /**
     *
     *
     * @var bool
     */
    protected $porterSecurity;
    /**
     *
     *
     * @var UpdatePricing
     */
    protected $pricing;
    /**
     *
     *
     * @var string|string
     */
    protected $propertyType;
    /**
     *
     *
     * @var float
     */
    protected $rateableValue;
    /**
     *
     *
     * @var object|string
     */
    protected $rentalTerm;
    /**
     *
     *
     * @var bool
     */
    protected $repossession;
    /**
     *
     *
     * @var bool
     */
    protected $retirement;
    /**
     *
     *
     * @var int
     */
    protected $sapRating;
    /**
     *
     *
     * @var UpdateServiceCharge
     */
    protected $serviceCharge;
    /**
     *
     *
     * @var bool
     */
    protected $serviced;
    /**
     *
     *
     * @var bool
     */
    protected $sharedAccommodation;
    /**
     *
     *
     * @var string
     */
    protected $summaryDescription;
    /**
     *
     *
     * @var bool
     */
    protected $swimmingPool;
    /**
     *
     *
     * @var bool
     */
    protected $tenanted;
    /**
     *
     *
     * @var UpdateTenantEligibility
     */
    protected $tenantEligibility;
    /**
     *
     *
     * @var bool
     */
    protected $tennisCourt;
    /**
     *
     *
     * @var mixed
     */
    protected $tenure;
    /**
     *
     *
     * @var int
     */
    protected $totalBedrooms;
    /**
     *
     *
     * @var bool
     */
    protected $utilityRoom;
    /**
     *
     *
     * @var bool
     */
    protected $waterfront;
    /**
     *
     *
     * @var bool
     */
    protected $woodFloors;

    /**
     *
     *
     * @return bool|null
     */
    public function getAccessibility(): ?bool
    {
        return $this->accessibility;
    }

    /**
     *
     *
     * @param bool|null $accessibility
     *
     * @return self
     */
    public function setAccessibility(?bool $accessibility): self
    {
        $this->accessibility = $accessibility;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getAdministrationFees(): ?string
    {
        return $this->administrationFees;
    }

    /**
     *
     *
     * @param string|null $administrationFees
     *
     * @return self
     */
    public function setAdministrationFees(?string $administrationFees): self
    {
        $this->administrationFees = $administrationFees;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getAnnualBusinessRates(): ?float
    {
        return $this->annualBusinessRates;
    }

    /**
     *
     *
     * @param float|null $annualBusinessRates
     *
     * @return self
     */
    public function setAnnualBusinessRates(?float $annualBusinessRates): self
    {
        $this->annualBusinessRates = $annualBusinessRates;
        return $this;
    }

    /**
     *
     *
     * @return UpdateAreas|null
     */
    public function getAreas(): ?UpdateAreas
    {
        return $this->areas;
    }

    /**
     *
     *
     * @param UpdateAreas|null $areas
     *
     * @return self
     */
    public function setAreas(?UpdateAreas $areas): self
    {
        $this->areas = $areas;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getAvailableBedrooms(): ?int
    {
        return $this->availableBedrooms;
    }

    /**
     *
     *
     * @param int|null $availableBedrooms
     *
     * @return self
     */
    public function setAvailableBedrooms(?int $availableBedrooms): self
    {
        $this->availableBedrooms = $availableBedrooms;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getAvailableFromDate(): ?string
    {
        return $this->availableFromDate;
    }

    /**
     *
     *
     * @param string|null $availableFromDate
     *
     * @return self
     */
    public function setAvailableFromDate(?string $availableFromDate): self
    {
        $this->availableFromDate = $availableFromDate;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getBasement(): ?bool
    {
        return $this->basement;
    }

    /**
     *
     *
     * @param bool|null $basement
     *
     * @return self
     */
    public function setBasement(?bool $basement): self
    {
        $this->basement = $basement;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getBathrooms(): ?int
    {
        return $this->bathrooms;
    }

    /**
     *
     *
     * @param int|null $bathrooms
     *
     * @return self
     */
    public function setBathrooms(?int $bathrooms): self
    {
        $this->bathrooms = $bathrooms;
        return $this;
    }

    /**
     *
     *
     * @return mixed[]|null
     */
    public function getBillsIncluded(): ?array
    {
        return $this->billsIncluded;
    }

    /**
     *
     *
     * @param mixed[]|null $billsIncluded
     *
     * @return self
     */
    public function setBillsIncluded(?array $billsIncluded): self
    {
        $this->billsIncluded = $billsIncluded;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getBranchReference(): ?string
    {
        return $this->branchReference;
    }

    /**
     *
     *
     * @param string|null $branchReference
     *
     * @return self
     */
    public function setBranchReference(?string $branchReference): self
    {
        $this->branchReference = $branchReference;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getBurglarAlarm(): ?bool
    {
        return $this->burglarAlarm;
    }

    /**
     *
     *
     * @param bool|null $burglarAlarm
     *
     * @return self
     */
    public function setBurglarAlarm(?bool $burglarAlarm): self
    {
        $this->burglarAlarm = $burglarAlarm;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getBusinessForSale(): ?bool
    {
        return $this->businessForSale;
    }

    /**
     *
     *
     * @param bool|null $businessForSale
     *
     * @return self
     */
    public function setBusinessForSale(?bool $businessForSale): self
    {
        $this->businessForSale = $businessForSale;
        return $this;
    }

    /**
     *
     *
     * @return mixed[]|null
     */
    public function getBuyerIncentives(): ?array
    {
        return $this->buyerIncentives;
    }

    /**
     *
     *
     * @param mixed[]|null $buyerIncentives
     *
     * @return self
     */
    public function setBuyerIncentives(?array $buyerIncentives): self
    {
        $this->buyerIncentives = $buyerIncentives;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     *
     *
     * @param mixed $category
     *
     * @return self
     */
    public function setCategory($category): self
    {
        $this->category = $category;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getCentralHeating()
    {
        return $this->centralHeating;
    }

    /**
     *
     *
     * @param mixed $centralHeating
     *
     * @return self
     */
    public function setCentralHeating($centralHeating): self
    {
        $this->centralHeating = $centralHeating;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getChainFree(): ?bool
    {
        return $this->chainFree;
    }

    /**
     *
     *
     * @param bool|null $chainFree
     *
     * @return self
     */
    public function setChainFree(?bool $chainFree): self
    {
        $this->chainFree = $chainFree;
        return $this;
    }

    /**
     *
     *
     * @return string[]|null
     */
    public function getCommercialUseClasses(): ?array
    {
        return $this->commercialUseClasses;
    }

    /**
     *
     *
     * @param string[]|null $commercialUseClasses
     *
     * @return self
     */
    public function setCommercialUseClasses(?array $commercialUseClasses): self
    {
        $this->commercialUseClasses = $commercialUseClasses;
        return $this;
    }

    /**
     *
     *
     * @return mixed[]|null
     */
    public function getConnectedUtilities(): ?array
    {
        return $this->connectedUtilities;
    }

    /**
     *
     *
     * @param mixed[]|null $connectedUtilities
     *
     * @return self
     */
    public function setConnectedUtilities(?array $connectedUtilities): self
    {
        $this->connectedUtilities = $connectedUtilities;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getConservatory(): ?bool
    {
        return $this->conservatory;
    }

    /**
     *
     *
     * @param bool|null $conservatory
     *
     * @return self
     */
    public function setConservatory(?bool $conservatory): self
    {
        $this->conservatory = $conservatory;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getConstructionYear(): ?int
    {
        return $this->constructionYear;
    }

    /**
     *
     *
     * @param int|null $constructionYear
     *
     * @return self
     */
    public function setConstructionYear(?int $constructionYear): self
    {
        $this->constructionYear = $constructionYear;
        return $this;
    }

    /**
     *
     *
     * @return UpdateContentItem[]|null
     */
    public function getContent(): ?array
    {
        return $this->content;
    }

    /**
     *
     *
     * @param UpdateContentItem[]|null $content
     *
     * @return self
     */
    public function setContent(?array $content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getCouncilTaxBand(): ?string
    {
        return $this->councilTaxBand;
    }

    /**
     *
     *
     * @param string|null $councilTaxBand
     *
     * @return self
     */
    public function setCouncilTaxBand(?string $councilTaxBand): self
    {
        $this->councilTaxBand = $councilTaxBand;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getDecorativeCondition()
    {
        return $this->decorativeCondition;
    }

    /**
     *
     *
     * @param mixed $decorativeCondition
     *
     * @return self
     */
    public function setDecorativeCondition($decorativeCondition): self
    {
        $this->decorativeCondition = $decorativeCondition;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getDeposit(): ?float
    {
        return $this->deposit;
    }

    /**
     *
     *
     * @param float|null $deposit
     *
     * @return self
     */
    public function setDeposit(?float $deposit): self
    {
        $this->deposit = $deposit;
        return $this;
    }

    /**
     *
     *
     * @return UpdateDetailedDescriptionItem[]|null
     */
    public function getDetailedDescription(): ?array
    {
        return $this->detailedDescription;
    }

    /**
     *
     *
     * @param UpdateDetailedDescriptionItem[]|null $detailedDescription
     *
     * @return self
     */
    public function setDetailedDescription(?array $detailedDescription): self
    {
        $this->detailedDescription = $detailedDescription;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getDisplayAddress(): ?string
    {
        return $this->displayAddress;
    }

    /**
     *
     *
     * @param string|null $displayAddress
     *
     * @return self
     */
    public function setDisplayAddress(?string $displayAddress): self
    {
        $this->displayAddress = $displayAddress;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getDoubleGlazing(): ?bool
    {
        return $this->doubleGlazing;
    }

    /**
     *
     *
     * @param bool|null $doubleGlazing
     *
     * @return self
     */
    public function setDoubleGlazing(?bool $doubleGlazing): self
    {
        $this->doubleGlazing = $doubleGlazing;
        return $this;
    }

    /**
     *
     *
     * @return UpdateEpcRatings|null
     */
    public function getEpcRatings(): ?UpdateEpcRatings
    {
        return $this->epcRatings;
    }

    /**
     *
     *
     * @param UpdateEpcRatings|null $epcRatings
     *
     * @return self
     */
    public function setEpcRatings(?UpdateEpcRatings $epcRatings): self
    {
        $this->epcRatings = $epcRatings;
        return $this;
    }

    /**
     *
     *
     * @return string[]|null
     */
    public function getFeatureList(): ?array
    {
        return $this->featureList;
    }

    /**
     *
     *
     * @param string[]|null $featureList
     *
     * @return self
     */
    public function setFeatureList(?array $featureList): self
    {
        $this->featureList = $featureList;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getFireplace(): ?bool
    {
        return $this->fireplace;
    }

    /**
     *
     *
     * @param bool|null $fireplace
     *
     * @return self
     */
    public function setFireplace(?bool $fireplace): self
    {
        $this->fireplace = $fireplace;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getFishingRights(): ?bool
    {
        return $this->fishingRights;
    }

    /**
     *
     *
     * @param bool|null $fishingRights
     *
     * @return self
     */
    public function setFishingRights(?bool $fishingRights): self
    {
        $this->fishingRights = $fishingRights;
        return $this;
    }

    /**
     *
     *
     * @return int[]|string[]|null
     */
    public function getFloorLevels(): ?array
    {
        return $this->floorLevels;
    }

    /**
     *
     *
     * @param int[]|string[]|null $floorLevels
     *
     * @return self
     */
    public function setFloorLevels(?array $floorLevels): self
    {
        $this->floorLevels = $floorLevels;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getFloors(): ?int
    {
        return $this->floors;
    }

    /**
     *
     *
     * @param int|null $floors
     *
     * @return self
     */
    public function setFloors(?int $floors): self
    {
        $this->floors = $floors;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getFurnishedState()
    {
        return $this->furnishedState;
    }

    /**
     *
     *
     * @param mixed $furnishedState
     *
     * @return self
     */
    public function setFurnishedState($furnishedState): self
    {
        $this->furnishedState = $furnishedState;
        return $this;
    }

    /**
     *
     *
     * @return UpdateGoogleStreetView|null
     */
    public function getGoogleStreetView(): ?UpdateGoogleStreetView
    {
        return $this->googleStreetView;
    }

    /**
     *
     *
     * @param UpdateGoogleStreetView|null $googleStreetView
     *
     * @return self
     */
    public function setGoogleStreetView(?UpdateGoogleStreetView $googleStreetView): self
    {
        $this->googleStreetView = $googleStreetView;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getGroundRent(): ?float
    {
        return $this->groundRent;
    }

    /**
     *
     *
     * @param float|null $groundRent
     *
     * @return self
     */
    public function setGroundRent(?float $groundRent): self
    {
        $this->groundRent = $groundRent;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getGym(): ?bool
    {
        return $this->gym;
    }

    /**
     *
     *
     * @param bool|null $gym
     *
     * @return self
     */
    public function setGym(?bool $gym): self
    {
        $this->gym = $gym;
        return $this;
    }

    /**
     *
     *
     * @return UpdateLeaseExpiry|null
     */
    public function getLeaseExpiry(): ?UpdateLeaseExpiry
    {
        return $this->leaseExpiry;
    }

    /**
     *
     *
     * @param UpdateLeaseExpiry|null $leaseExpiry
     *
     * @return self
     */
    public function setLeaseExpiry(?UpdateLeaseExpiry $leaseExpiry): self
    {
        $this->leaseExpiry = $leaseExpiry;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getLifeCycleStatus()
    {
        return $this->lifeCycleStatus;
    }

    /**
     *
     *
     * @param mixed $lifeCycleStatus
     *
     * @return self
     */
    public function setLifeCycleStatus($lifeCycleStatus): self
    {
        $this->lifeCycleStatus = $lifeCycleStatus;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getListedBuildingGrade()
    {
        return $this->listedBuildingGrade;
    }

    /**
     *
     *
     * @param mixed $listedBuildingGrade
     *
     * @return self
     */
    public function setListedBuildingGrade($listedBuildingGrade): self
    {
        $this->listedBuildingGrade = $listedBuildingGrade;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getListingReference(): ?string
    {
        return $this->listingReference;
    }

    /**
     *
     *
     * @param string|null $listingReference
     *
     * @return self
     */
    public function setListingReference(?string $listingReference): self
    {
        $this->listingReference = $listingReference;
        return $this;
    }

    /**
     *
     *
     * @return UpdateLocation|null
     */
    public function getLocation(): ?UpdateLocation
    {
        return $this->location;
    }

    /**
     *
     *
     * @param UpdateLocation|null $location
     *
     * @return self
     */
    public function setLocation(?UpdateLocation $location): self
    {
        $this->location = $location;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getLivingRooms(): ?int
    {
        return $this->livingRooms;
    }

    /**
     *
     *
     * @param int|null $livingRooms
     *
     * @return self
     */
    public function setLivingRooms(?int $livingRooms): self
    {
        $this->livingRooms = $livingRooms;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getLoft(): ?bool
    {
        return $this->loft;
    }

    /**
     *
     *
     * @param bool|null $loft
     *
     * @return self
     */
    public function setLoft(?bool $loft): self
    {
        $this->loft = $loft;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getNewHome(): ?bool
    {
        return $this->newHome;
    }

    /**
     *
     *
     * @param bool|null $newHome
     *
     * @return self
     */
    public function setNewHome(?bool $newHome): self
    {
        $this->newHome = $newHome;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getOpenDay(): ?string
    {
        return $this->openDay;
    }

    /**
     *
     *
     * @param string|null $openDay
     *
     * @return self
     */
    public function setOpenDay(?string $openDay): self
    {
        $this->openDay = $openDay;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getOutbuildings(): ?bool
    {
        return $this->outbuildings;
    }

    /**
     *
     *
     * @param bool|null $outbuildings
     *
     * @return self
     */
    public function setOutbuildings(?bool $outbuildings): self
    {
        $this->outbuildings = $outbuildings;
        return $this;
    }

    /**
     *
     *
     * @return mixed[]|null
     */
    public function getOutsideSpace(): ?array
    {
        return $this->outsideSpace;
    }

    /**
     *
     *
     * @param mixed[]|null $outsideSpace
     *
     * @return self
     */
    public function setOutsideSpace(?array $outsideSpace): self
    {
        $this->outsideSpace = $outsideSpace;
        return $this;
    }

    /**
     *
     *
     * @return mixed[]|null
     */
    public function getParking(): ?array
    {
        return $this->parking;
    }

    /**
     *
     *
     * @param mixed[]|null $parking
     *
     * @return self
     */
    public function setParking(?array $parking): self
    {
        $this->parking = $parking;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getPetsAllowed(): ?bool
    {
        return $this->petsAllowed;
    }

    /**
     *
     *
     * @param bool|null $petsAllowed
     *
     * @return self
     */
    public function setPetsAllowed(?bool $petsAllowed): self
    {
        $this->petsAllowed = $petsAllowed;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getPorterSecurity(): ?bool
    {
        return $this->porterSecurity;
    }

    /**
     *
     *
     * @param bool|null $porterSecurity
     *
     * @return self
     */
    public function setPorterSecurity(?bool $porterSecurity): self
    {
        $this->porterSecurity = $porterSecurity;
        return $this;
    }

    /**
     *
     *
     * @return UpdatePricing|null
     */
    public function getPricing(): ?UpdatePricing
    {
        return $this->pricing;
    }

    /**
     *
     *
     * @param UpdatePricing|null $pricing
     *
     * @return self
     */
    public function setPricing(?UpdatePricing $pricing): self
    {
        $this->pricing = $pricing;
        return $this;
    }

    /**
     *
     *
     * @return string|string|null
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     *
     *
     * @param string|string|null $propertyType
     *
     * @return self
     */
    public function setPropertyType($propertyType): self
    {
        $this->propertyType = $propertyType;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getRateableValue(): ?float
    {
        return $this->rateableValue;
    }

    /**
     *
     *
     * @param float|null $rateableValue
     *
     * @return self
     */
    public function setRateableValue(?float $rateableValue): self
    {
        $this->rateableValue = $rateableValue;
        return $this;
    }

    /**
     *
     *
     * @return object|string|null
     */
    public function getRentalTerm()
    {
        return $this->rentalTerm;
    }

    /**
     *
     *
     * @param object|string|null $rentalTerm
     *
     * @return self
     */
    public function setRentalTerm($rentalTerm): self
    {
        $this->rentalTerm = $rentalTerm;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getRepossession(): ?bool
    {
        return $this->repossession;
    }

    /**
     *
     *
     * @param bool|null $repossession
     *
     * @return self
     */
    public function setRepossession(?bool $repossession): self
    {
        $this->repossession = $repossession;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getRetirement(): ?bool
    {
        return $this->retirement;
    }

    /**
     *
     *
     * @param bool|null $retirement
     *
     * @return self
     */
    public function setRetirement(?bool $retirement): self
    {
        $this->retirement = $retirement;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getSapRating(): ?int
    {
        return $this->sapRating;
    }

    /**
     *
     *
     * @param int|null $sapRating
     *
     * @return self
     */
    public function setSapRating(?int $sapRating): self
    {
        $this->sapRating = $sapRating;
        return $this;
    }

    /**
     *
     *
     * @return UpdateServiceCharge|null
     */
    public function getServiceCharge(): ?UpdateServiceCharge
    {
        return $this->serviceCharge;
    }

    /**
     *
     *
     * @param UpdateServiceCharge|null $serviceCharge
     *
     * @return self
     */
    public function setServiceCharge(?UpdateServiceCharge $serviceCharge): self
    {
        $this->serviceCharge = $serviceCharge;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getServiced(): ?bool
    {
        return $this->serviced;
    }

    /**
     *
     *
     * @param bool|null $serviced
     *
     * @return self
     */
    public function setServiced(?bool $serviced): self
    {
        $this->serviced = $serviced;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getSharedAccommodation(): ?bool
    {
        return $this->sharedAccommodation;
    }

    /**
     *
     *
     * @param bool|null $sharedAccommodation
     *
     * @return self
     */
    public function setSharedAccommodation(?bool $sharedAccommodation): self
    {
        $this->sharedAccommodation = $sharedAccommodation;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getSummaryDescription(): ?string
    {
        return $this->summaryDescription;
    }

    /**
     *
     *
     * @param string|null $summaryDescription
     *
     * @return self
     */
    public function setSummaryDescription(?string $summaryDescription): self
    {
        $this->summaryDescription = $summaryDescription;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getSwimmingPool(): ?bool
    {
        return $this->swimmingPool;
    }

    /**
     *
     *
     * @param bool|null $swimmingPool
     *
     * @return self
     */
    public function setSwimmingPool(?bool $swimmingPool): self
    {
        $this->swimmingPool = $swimmingPool;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getTenanted(): ?bool
    {
        return $this->tenanted;
    }

    /**
     *
     *
     * @param bool|null $tenanted
     *
     * @return self
     */
    public function setTenanted(?bool $tenanted): self
    {
        $this->tenanted = $tenanted;
        return $this;
    }

    /**
     *
     *
     * @return UpdateTenantEligibility|null
     */
    public function getTenantEligibility(): ?UpdateTenantEligibility
    {
        return $this->tenantEligibility;
    }

    /**
     *
     *
     * @param UpdateTenantEligibility|null $tenantEligibility
     *
     * @return self
     */
    public function setTenantEligibility(?UpdateTenantEligibility $tenantEligibility): self
    {
        $this->tenantEligibility = $tenantEligibility;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getTennisCourt(): ?bool
    {
        return $this->tennisCourt;
    }

    /**
     *
     *
     * @param bool|null $tennisCourt
     *
     * @return self
     */
    public function setTennisCourt(?bool $tennisCourt): self
    {
        $this->tennisCourt = $tennisCourt;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getTenure()
    {
        return $this->tenure;
    }

    /**
     *
     *
     * @param mixed $tenure
     *
     * @return self
     */
    public function setTenure($tenure): self
    {
        $this->tenure = $tenure;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getTotalBedrooms(): ?int
    {
        return $this->totalBedrooms;
    }

    /**
     *
     *
     * @param int|null $totalBedrooms
     *
     * @return self
     */
    public function setTotalBedrooms(?int $totalBedrooms): self
    {
        $this->totalBedrooms = $totalBedrooms;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getUtilityRoom(): ?bool
    {
        return $this->utilityRoom;
    }

    /**
     *
     *
     * @param bool|null $utilityRoom
     *
     * @return self
     */
    public function setUtilityRoom(?bool $utilityRoom): self
    {
        $this->utilityRoom = $utilityRoom;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getWaterfront(): ?bool
    {
        return $this->waterfront;
    }

    /**
     *
     *
     * @param bool|null $waterfront
     *
     * @return self
     */
    public function setWaterfront(?bool $waterfront): self
    {
        $this->waterfront = $waterfront;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getWoodFloors(): ?bool
    {
        return $this->woodFloors;
    }

    /**
     *
     *
     * @param bool|null $woodFloors
     *
     * @return self
     */
    public function setWoodFloors(?bool $woodFloors): self
    {
        $this->woodFloors = $woodFloors;
        return $this;
    }
}
