<?php

namespace Zpg\Model;

class Overseas
{
    /**
     *
     *
     * @var mixed
     */
    protected $location;

    /**
     *
     *
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     *
     *
     * @param mixed $location
     *
     * @return self
     */
    public function setLocation($location): self
    {
        $this->location = $location;
        return $this;
    }
}
