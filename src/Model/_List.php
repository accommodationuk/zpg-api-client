<?php

namespace Zpg\Model;

class _List
{
    /**
     *
     *
     * @var string
     */
    protected $branchReference;

    /**
     *
     *
     * @return string|null
     */
    public function getBranchReference(): ?string
    {
        return $this->branchReference;
    }

    /**
     *
     *
     * @param string|null $branchReference
     *
     * @return self
     */
    public function setBranchReference(?string $branchReference): self
    {
        $this->branchReference = $branchReference;
        return $this;
    }
}
