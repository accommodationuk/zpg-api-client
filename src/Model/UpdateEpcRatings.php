<?php

namespace Zpg\Model;

class UpdateEpcRatings
{
    /**
     *
     *
     * @var int
     */
    protected $eerCurrentRating;
    /**
     *
     *
     * @var int
     */
    protected $eerPotentialRating;
    /**
     *
     *
     * @var int
     */
    protected $eirCurrentRating;
    /**
     *
     *
     * @var int
     */
    protected $eirPotentialRating;

    /**
     *
     *
     * @return int|null
     */
    public function getEerCurrentRating(): ?int
    {
        return $this->eerCurrentRating;
    }

    /**
     *
     *
     * @param int|null $eerCurrentRating
     *
     * @return self
     */
    public function setEerCurrentRating(?int $eerCurrentRating): self
    {
        $this->eerCurrentRating = $eerCurrentRating;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getEerPotentialRating(): ?int
    {
        return $this->eerPotentialRating;
    }

    /**
     *
     *
     * @param int|null $eerPotentialRating
     *
     * @return self
     */
    public function setEerPotentialRating(?int $eerPotentialRating): self
    {
        $this->eerPotentialRating = $eerPotentialRating;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getEirCurrentRating(): ?int
    {
        return $this->eirCurrentRating;
    }

    /**
     *
     *
     * @param int|null $eirCurrentRating
     *
     * @return self
     */
    public function setEirCurrentRating(?int $eirCurrentRating): self
    {
        $this->eirCurrentRating = $eirCurrentRating;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getEirPotentialRating(): ?int
    {
        return $this->eirPotentialRating;
    }

    /**
     *
     *
     * @param int|null $eirPotentialRating
     *
     * @return self
     */
    public function setEirPotentialRating(?int $eirPotentialRating): self
    {
        $this->eirPotentialRating = $eirPotentialRating;
        return $this;
    }
}
