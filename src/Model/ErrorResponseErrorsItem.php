<?php

namespace Zpg\Model;

class ErrorResponseErrorsItem
{
    /**
     *
     *
     * @var string
     */
    protected $message;
    /**
     *
     *
     * @var string
     */
    protected $path;

    /**
     *
     *
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     *
     *
     * @param string|null $message
     *
     * @return self
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     *
     *
     * @param string|null $path
     *
     * @return self
     */
    public function setPath(?string $path): self
    {
        $this->path = $path;
        return $this;
    }
}
