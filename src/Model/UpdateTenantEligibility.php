<?php

namespace Zpg\Model;

class UpdateTenantEligibility
{
    public const DSS_ACCEPTED = 'accepted', DSS_EXCLUDED = 'excluded', DSS_ONLY = 'only';
    public const STUDENTS_ACCEPTED = 'accepted', STUDENTS_EXCLUDED = 'excluded', STUDENTS_ONLY = 'only';
    /**
     *
     *
     * @var mixed
     */
    protected $dss;
    /**
     *
     *
     * @var mixed
     */
    protected $students;

    /**
     *
     *
     * @return mixed
     */
    public function getDss()
    {
        return $this->dss;
    }

    /**
     *
     *
     * @param mixed $dss
     *
     * @return self
     */
    public function setDss($dss): self
    {
        $this->dss = $dss;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     *
     *
     * @param mixed $students
     *
     * @return self
     */
    public function setStudents($students): self
    {
        $this->students = $students;
        return $this;
    }
}
