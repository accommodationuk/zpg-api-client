<?php

namespace Zpg\Model;

class UpdateServiceCharge
{
    /**
     *
     *
     * @var float
     */
    protected $charge;
    /**
     *
     *
     * @var mixed
     */
    protected $perUnitAreaUnits;
    /**
     *
     *
     * @var mixed
     */
    protected $frequency;

    /**
     *
     *
     * @return float|null
     */
    public function getCharge(): ?float
    {
        return $this->charge;
    }

    /**
     *
     *
     * @param float|null $charge
     *
     * @return self
     */
    public function setCharge(?float $charge): self
    {
        $this->charge = $charge;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getPerUnitAreaUnits()
    {
        return $this->perUnitAreaUnits;
    }

    /**
     *
     *
     * @param mixed $perUnitAreaUnits
     *
     * @return self
     */
    public function setPerUnitAreaUnits($perUnitAreaUnits): self
    {
        $this->perUnitAreaUnits = $perUnitAreaUnits;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     *
     *
     * @param mixed $frequency
     *
     * @return self
     */
    public function setFrequency($frequency): self
    {
        $this->frequency = $frequency;
        return $this;
    }
}
