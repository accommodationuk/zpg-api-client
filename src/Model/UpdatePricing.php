<?php

namespace Zpg\Model;

class UpdatePricing
{
    public const PRICE_QUALIFIER_FIXED_PRICE = 'fixed_price', PRICE_QUALIFIER_FROM = 'from', PRICE_QUALIFIER_GUIDE_PRICE = 'guide_price', PRICE_QUALIFIER_NON_QUOTING = 'non_quoting', PRICE_QUALIFIER_OFFERS_IN_THE_REGION_OF = 'offers_in_the_region_of', PRICE_QUALIFIER_OFFERS_OVER = 'offers_over', PRICE_QUALIFIER_PRICE_ON_APPLICATION = 'price_on_application', PRICE_QUALIFIER_SALE_BY_TENDER = 'sale_by_tender';
    public const TRANSACTION_TYPE_RENT = 'rent', TRANSACTION_TYPE_SALE = 'sale';
    /**
     *
     *
     * @var bool
     */
    protected $auction;
    /**
     *
     *
     * @var string
     */
    protected $currencyCode;
    /**
     *
     *
     * @var float
     */
    protected $price;
    /**
     *
     *
     * @var UpdatePricingPricePerUnitArea
     */
    protected $pricePerUnitArea;
    /**
     *
     *
     * @var mixed
     */
    protected $priceQualifier;
    /**
     *
     *
     * @var mixed
     */
    protected $rentFrequency;
    /**
     *
     *
     * @var mixed
     */
    protected $transactionType;

    /**
     *
     *
     * @return bool|null
     */
    public function getAuction(): ?bool
    {
        return $this->auction;
    }

    /**
     *
     *
     * @param bool|null $auction
     *
     * @return self
     */
    public function setAuction(?bool $auction): self
    {
        $this->auction = $auction;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     *
     *
     * @param string|null $currencyCode
     *
     * @return self
     */
    public function setCurrencyCode(?string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     *
     *
     * @param float|null $price
     *
     * @return self
     */
    public function setPrice(?float $price): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     *
     *
     * @return UpdatePricingPricePerUnitArea|null
     */
    public function getPricePerUnitArea(): ?UpdatePricingPricePerUnitArea
    {
        return $this->pricePerUnitArea;
    }

    /**
     *
     *
     * @param UpdatePricingPricePerUnitArea|null $pricePerUnitArea
     *
     * @return self
     */
    public function setPricePerUnitArea(?UpdatePricingPricePerUnitArea $pricePerUnitArea): self
    {
        $this->pricePerUnitArea = $pricePerUnitArea;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getPriceQualifier()
    {
        return $this->priceQualifier;
    }

    /**
     *
     *
     * @param mixed $priceQualifier
     *
     * @return self
     */
    public function setPriceQualifier($priceQualifier): self
    {
        $this->priceQualifier = $priceQualifier;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getRentFrequency()
    {
        return $this->rentFrequency;
    }

    /**
     *
     *
     * @param mixed $rentFrequency
     *
     * @return self
     */
    public function setRentFrequency($rentFrequency): self
    {
        $this->rentFrequency = $rentFrequency;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     *
     *
     * @param mixed $transactionType
     *
     * @return self
     */
    public function setTransactionType($transactionType): self
    {
        $this->transactionType = $transactionType;
        return $this;
    }
}
