<?php

namespace Zpg\Model;

class UpdateContentItem
{
    public const TYPE_AUDIO_TOUR = 'audio_tour', TYPE_BROCHURE = 'brochure', TYPE_DOCUMENT = 'document', TYPE_EPC_GRAPH = 'epc_graph', TYPE_EPC_REPORT = 'epc_report', TYPE_FLOOR_PLAN = 'floor_plan', TYPE_HOME_PACK = 'home_pack', TYPE_IMAGE = 'image', TYPE_SITE_PLAN = 'site_plan', TYPE_VIRTUAL_TOUR = 'virtual_tour';
    /**
     *
     *
     * @var string
     */
    protected $caption;
    /**
     *
     *
     * @var mixed
     */
    protected $type;
    /**
     *
     *
     * @var string
     */
    protected $url;

    /**
     *
     *
     * @return string|null
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     *
     *
     * @param string|null $caption
     *
     * @return self
     */
    public function setCaption(?string $caption): self
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     *
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     *
     *
     * @param string|null $url
     *
     * @return self
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }
}
