<?php

namespace Zpg\Model;

class MinMaxArea
{
    /**
     *
     *
     * @var Area
     */
    protected $minimum;
    /**
     *
     *
     * @var Area
     */
    protected $maximum;

    /**
     *
     *
     * @return Area|null
     */
    public function getMinimum(): ?Area
    {
        return $this->minimum;
    }

    /**
     *
     *
     * @param Area|null $minimum
     *
     * @return self
     */
    public function setMinimum(?Area $minimum): self
    {
        $this->minimum = $minimum;
        return $this;
    }

    /**
     *
     *
     * @return Area|null
     */
    public function getMaximum(): ?Area
    {
        return $this->maximum;
    }

    /**
     *
     *
     * @param Area|null $maximum
     *
     * @return self
     */
    public function setMaximum(?Area $maximum): self
    {
        $this->maximum = $maximum;
        return $this;
    }
}
